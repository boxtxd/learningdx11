#include <windows.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <DirectXColors.h>


using namespace DirectX;

/*
* Structures
*/

struct VertexX {
	XMFLOAT3 position;
};

struct VertexXR {
	XMFLOAT3 position;
	XMFLOAT4 color;
};

struct VertexXN {
	XMFLOAT3 position;
	XMFLOAT3 normal;
};

struct ConstantBuffer {
	XMMATRIX mWorld;
	XMMATRIX mView;
	XMMATRIX mPprojection;
	XMFLOAT4 vLightDir[2];
	XMFLOAT4 vLightColor[2];
	XMFLOAT4 vOutputColor;
};

/*
* Global variables
*/

//vars for window 
HINSTANCE				g_inst = nullptr;
HWND					g_hwnd = nullptr;
D3D_DRIVER_TYPE			g_dxDriverType = D3D_DRIVER_TYPE_NULL;
D3D_FEATURE_LEVEL		g_dxFeatureLevel = D3D_FEATURE_LEVEL_11_0;
ID3D11Device*			g_pdxDevice = nullptr;
ID3D11Device1*			g_pdxDevice1 = nullptr;
ID3D11DeviceContext*	g_pdxContext = nullptr;
ID3D11DeviceContext1*	g_pdxContext1 = nullptr;
IDXGISwapChain*			g_pdxSwapChain = nullptr;
IDXGISwapChain1*		g_pdxSwapChain1 = nullptr;

//targets
ID3D11RenderTargetView* g_pdxRenderTargetView = nullptr;
ID3D11Texture2D*		g_pDepthStencil = nullptr;
ID3D11DepthStencilView*	g_pDepthStencilView = nullptr;

//vars for drawing
ID3D11VertexShader*		g_pVertexShader = nullptr;
ID3D11PixelShader*		g_pPixelShader = nullptr;
ID3D11PixelShader*		g_pPixelShaderSolid = nullptr;
ID3D11InputLayout*		g_pVertexLayout = nullptr;
ID3D11Buffer*			g_pVertexBuffer = nullptr;
ID3D11Buffer*			g_pIndexBuffer = nullptr;
ID3D11Buffer*			g_pConstantBuffer = nullptr;

XMMATRIX			g_mWorld;
XMMATRIX			g_mView;
XMMATRIX			g_mProjection;

XMFLOAT4			g_vLightDirs[2];
XMFLOAT4			g_vLightColors[2];

/*
* Forward Declarations
*/

HRESULT initWindow(HINSTANCE hInstance, int nCmdShow);
HRESULT initDevice();
void dispose();
LRESULT CALLBACK wndProc(HWND, UINT, WPARAM, LPARAM);
void render();
void update();

HRESULT createShaders();
HRESULT compileShaderFromFile(LPCWSTR, LPCSTR, LPCSTR, ID3DBlob**);
HRESULT createBuffers();

/*
* function definitions
*/

HRESULT compileShaderFromFile(LPCWSTR szFilename, LPCSTR szEntryPointName, LPCSTR szShaderModel, ID3DBlob** ppBlobOut) {
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

#ifdef _DEBUG
	//enabline debug information for the shader
	dwShaderFlags |= D3DCOMPILE_DEBUG;

	//disabling optimizations too... more information 
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif // _DEBUG
	
	ID3DBlob* pErrorBlob = nullptr;
	hr = D3DCompileFromFile(szFilename, nullptr, nullptr, szEntryPointName, szShaderModel, dwShaderFlags, 0, ppBlobOut, &pErrorBlob);
	if (FAILED(hr)) {
		if (pErrorBlob) {
			OutputDebugStringA(reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()));
			pErrorBlob->Release();
		}
		return hr;
	}
	if (pErrorBlob) pErrorBlob->Release();

	return S_OK;
}

/*
* Entry point
*/

int WINAPI wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrecInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow) {
	UNREFERENCED_PARAMETER(hPrecInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	if (FAILED(initWindow(hInstance, nCmdShow))) {
		return 0;
	}

	if (FAILED(initDevice())) {
		dispose();
		return 0;
	}

	if (FAILED(createShaders())) {
		dispose();
		return 0;
	}

	if (FAILED(createBuffers())) {
		dispose();
		return 0;
	}

	// message loop
	MSG msg = { 0 };

	while (WM_QUIT != msg.message) {
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			update();
			render();
		}
	}

	dispose();
	return (int)msg.wParam;
}

/*
* Registering class and creating window
*/

HRESULT initWindow(HINSTANCE hInstance, int nCmdShow) {
	//registering class
	WNDCLASSEX wndClass;
	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = wndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDC_ICON);
	wndClass.hCursor = LoadCursor(hInstance, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wndClass.lpszMenuName = nullptr;
	wndClass.lpszClassName = (LPCSTR)"FirstCube";
	wndClass.hIconSm = LoadIcon(wndClass.hInstance, IDC_ICON);

	if (!RegisterClassEx(&wndClass)) {
		return E_FAIL;
	}

	//creating window
	g_inst = hInstance;
	RECT rect = { 0, 0, 800, 600 };
	AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, FALSE);
	g_hwnd = CreateWindow((LPCSTR)"FirstCube", (LPCSTR)"D3x 11 First Cube",
		WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		CW_USEDEFAULT, CW_USEDEFAULT, rect.right - rect.left, rect.bottom - rect.top,
		nullptr, nullptr, hInstance, nullptr);

	if (!g_hwnd) {
		return E_FAIL;
	}

	ShowWindow(g_hwnd, nCmdShow);

	return S_OK;
}

/*
* Message processing
*/

LRESULT CALLBACK wndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

/*
* creating d3d device
*/

HRESULT initDevice() {
	HRESULT hr = S_OK;

	RECT rect;
	GetClientRect(g_hwnd, &rect);
	UINT width = rect.right - rect.left;
	UINT height = rect.bottom - rect.top;

	UINT createDeviceFlags = 0;

#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif // _DEBUG

	D3D_DRIVER_TYPE driverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] = {
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	for (UINT i = 0; i < numDriverTypes; i++) {
		g_dxDriverType = driverTypes[i];
		hr = D3D11CreateDevice(nullptr, g_dxDriverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels, D3D11_SDK_VERSION, &g_pdxDevice, &g_dxFeatureLevel, &g_pdxContext);

		if (hr == E_INVALIDARG) {
			hr = D3D11CreateDevice(nullptr, g_dxDriverType, nullptr, createDeviceFlags, &featureLevels[1], numFeatureLevels - 1, D3D11_SDK_VERSION, &g_pdxDevice, &g_dxFeatureLevel, &g_pdxContext);
		}

		if (SUCCEEDED(hr)) {
			break;
		}
	}
	if (FAILED(hr)) {
		return hr;
	}

	// obtaining DXGI factory from device - since we used nullptr for pAdapter above
	IDXGIFactory1* dxgiFactory = nullptr; 
	{
		IDXGIDevice* dxgiDevice = nullptr;
		hr = g_pdxDevice->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgiDevice));
		if (SUCCEEDED(hr)) {
			IDXGIAdapter* adapter = nullptr;
			hr = dxgiDevice->GetAdapter(&adapter);
			if (SUCCEEDED(hr)) {
				hr = adapter->GetParent(__uuidof(IDXGIFactory1), reinterpret_cast<void**>(&dxgiFactory));
				adapter->Release();
			}
			dxgiDevice->Release();
		}
	}
	if (FAILED(hr)) {
		return hr;
	}

	// creating swap chain
	IDXGIFactory2* dxgiFactory2 = nullptr;
	hr = dxgiFactory->QueryInterface(__uuidof(IDXGIFactory2), reinterpret_cast<void**>(&dxgiFactory2));
	if (dxgiFactory2) {
		//Dx11.1 or later
		hr = g_pdxDevice->QueryInterface(__uuidof(ID3D11Device1), reinterpret_cast<void**>(&g_pdxDevice1));
		if (SUCCEEDED(hr)) {
			(void)g_pdxContext->QueryInterface(__uuidof(ID3D11DeviceContext1), reinterpret_cast<void**>(&g_pdxContext1));
		}

		DXGI_SWAP_CHAIN_DESC1 sd = {};
		sd.Width = width;
		sd.Height = height;
		sd.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.BufferCount = 1;

		hr = dxgiFactory2->CreateSwapChainForHwnd(g_pdxDevice, g_hwnd, &sd, nullptr, nullptr, &g_pdxSwapChain1);
		if (SUCCEEDED(hr)) {
			hr = g_pdxSwapChain1->QueryInterface(__uuidof(IDXGISwapChain), reinterpret_cast<void**>(&g_pdxSwapChain));
		}
		dxgiFactory2->Release();
	}
	else {
		//Dx11 system
		DXGI_SWAP_CHAIN_DESC sd = {};
		sd.BufferCount = 1;
		sd.BufferDesc.Width = width;
		sd.BufferDesc.Height = height;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = g_hwnd;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = TRUE;

		hr = dxgiFactory->CreateSwapChain(g_pdxDevice, &sd, &g_pdxSwapChain);
	}

	//disabling ALT+ENTER shortcut for fullscreen
	dxgiFactory->MakeWindowAssociation(g_hwnd, DXGI_MWA_NO_ALT_ENTER);
	dxgiFactory->Release();

	if (FAILED(hr)) {
		return hr;
	}

	//creating render target
	ID3D11Texture2D* pBackBuffer = nullptr;
	hr = g_pdxSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer));
	if (FAILED(hr)) {
		return hr;
	}

	hr = g_pdxDevice->CreateRenderTargetView(pBackBuffer, nullptr, &g_pdxRenderTargetView);
	pBackBuffer->Release();
	if (FAILED(hr)) {
		return hr;
	}

	//creating depth stencil texture
	D3D11_TEXTURE2D_DESC depthDesc = {};
	depthDesc.Width = width;
	depthDesc.Height = height;
	depthDesc.MipLevels = 1;
	depthDesc.ArraySize = 1;
	depthDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthDesc.SampleDesc.Count = 1;
	depthDesc.SampleDesc.Quality = 0;
	depthDesc.Usage = D3D11_USAGE_DEFAULT;
	depthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthDesc.CPUAccessFlags = 0;
	depthDesc.MiscFlags = 0;
	hr = g_pdxDevice->CreateTexture2D(&depthDesc, nullptr, &g_pDepthStencil);
	if (FAILED(hr)) {
		return hr;
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC depthViewDesc = {};
	depthViewDesc.Format = depthDesc.Format;
	depthViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthViewDesc.Texture2D.MipSlice = 0;
	hr = g_pdxDevice->CreateDepthStencilView(g_pDepthStencil, &depthViewDesc, &g_pDepthStencilView);
	if (FAILED(hr)) {
		return hr;
	}

	g_pdxContext->OMSetRenderTargets(1, &g_pdxRenderTargetView, g_pDepthStencilView);

	//setting viewport
	D3D11_VIEWPORT vp;
	vp.Width = width;
	vp.Height = height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	g_pdxContext->RSSetViewports(1, &vp);
	
	return S_OK;
}

/*
* creating shaders
*/

HRESULT createShaders() {
	HRESULT hr = S_OK;

	//compiling and creating vertex shader
	ID3DBlob* pVsBlob = nullptr;
	hr = compileShaderFromFile(L"light.fx", "VS", "vs_4_0", &pVsBlob);
	if (FAILED(hr)) {
		MessageBox(nullptr, (LPCSTR)"The VS FX file cannot be compiled !!", (LPCSTR)"Error", MB_OK);
		return hr;
	}

	hr = g_pdxDevice->CreateVertexShader(pVsBlob->GetBufferPointer(), pVsBlob->GetBufferSize(), nullptr, &g_pVertexShader);
	if (FAILED(hr)) {
		pVsBlob->Release();
		return hr;
	}

	//defining the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
	};
	UINT numInputLayouts = ARRAYSIZE(layout);

	//creating the layout
	hr = g_pdxDevice->CreateInputLayout(layout, numInputLayouts, pVsBlob->GetBufferPointer(), pVsBlob->GetBufferSize(), &g_pVertexLayout);
	pVsBlob->Release();
	if (FAILED(hr)) {
		return hr;
	}

	//setting layout to the context
	g_pdxContext->IASetInputLayout(g_pVertexLayout);

	//compile pixel shader
	ID3DBlob* pPsBlob = nullptr;
	hr = compileShaderFromFile(L"light.fx", "PS", "ps_4_0", &pPsBlob);
	if (FAILED(hr)) {
		MessageBox(nullptr, (LPCSTR)"The PS FX file cannot be compiled !!", (LPCSTR)"Error", MB_OK);
		return hr;
	}

	hr = g_pdxDevice->CreatePixelShader(pPsBlob->GetBufferPointer(), pPsBlob->GetBufferSize(), nullptr, &g_pPixelShader);
	pPsBlob->Release();
	if (FAILED(hr)) {
		return hr;
	}

	//compile pixel shader solid
	pPsBlob = nullptr;
	hr = compileShaderFromFile(L"light.fx", "PSSolid", "ps_4_0", &pPsBlob);
	if (FAILED(hr)) {
		MessageBox(nullptr, (LPCSTR)"The PS FX file cannot be compiled !!", (LPCSTR)"Error", MB_OK);
		return hr;
	}

	hr = g_pdxDevice->CreatePixelShader(pPsBlob->GetBufferPointer(), pPsBlob->GetBufferSize(), nullptr, &g_pPixelShaderSolid);
	pPsBlob->Release();
	if (FAILED(hr)) {
		return hr;
	}
}

/*
* creating vertex buffers
*/

HRESULT createBuffers() {
	HRESULT hr = S_OK;

	//vertices
	VertexXN vertices[] =
	{
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, -1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, -1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT3(0.0f, -1.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT3(0.0f, -1.0f, 0.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f) },

		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, 0.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, -1.0f) },

		{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f) },
	};

	UINT numVertices = ARRAYSIZE(vertices);
	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.ByteWidth = sizeof(VertexXN) * numVertices;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA initData = {};
	initData.pSysMem = vertices;
	hr = g_pdxDevice->CreateBuffer(&bufferDesc, &initData, &g_pVertexBuffer);
	if (FAILED(hr)) {
		return hr;
	}

	//setting vertex buffer
	UINT stride = sizeof(VertexXN);
	UINT offset = 0;
	g_pdxContext->IASetVertexBuffers(0, 1, &g_pVertexBuffer, &stride, &offset);

	//indices
	WORD indices[] = {
		3,1,0,
		2,1,3,

		6,4,5,
		7,4,6,

		11,9,8,
		10,9,11,

		14,12,13,
		15,12,14,

		19,17,16,
		18,17,19,

		22,20,21,
		23,20,22
	};
	UINT numIndices = ARRAYSIZE(indices);
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.ByteWidth = sizeof(WORD) * numIndices;
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;

	initData.pSysMem = indices;
	hr = g_pdxDevice->CreateBuffer(&bufferDesc, &initData, &g_pIndexBuffer);
	if (FAILED(hr)) {
		return hr;
	}

	//setting index buffer
	g_pdxContext->IASetIndexBuffer(g_pIndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	//setting primitive topology
	g_pdxContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//creating constant buffer
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.ByteWidth = sizeof(ConstantBuffer);
	bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bufferDesc.CPUAccessFlags = 0;
	hr = g_pdxDevice->CreateBuffer(&bufferDesc, nullptr, &g_pConstantBuffer);
	if (FAILED(hr)) {
		return hr;
	}

	//creating constant buffer
	g_mWorld = XMMatrixIdentity();

	XMVECTOR eye = XMVectorSet(0.0f, 4.0f, -10.0f, 0.0f);
	XMVECTOR lookAt = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	g_mView = XMMatrixLookAtLH(eye, lookAt, up);

	g_mProjection = XMMatrixPerspectiveFovLH(XM_PIDIV4, 800 / (float)600, 0.01f, 100.0f);

	// setting lighting parameters
	g_vLightDirs[0] = XMFLOAT4(-0.577f, 0.577f, -0.577f, 1.0f);
	g_vLightDirs[1] = XMFLOAT4(0.0f, 0.0f, -1.0f, 1.0f);

	g_vLightColors[0] = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	g_vLightColors[1] = XMFLOAT4(0.5f, 0.0f, 0.0f, 1.0f);

	return S_OK;
}

/*
* simulate the environment
*/

void update() {
	//time calculation
	static float t = 0.0f;
	if (g_dxDriverType == D3D_DRIVER_TYPE_REFERENCE) {
		t += (float)XM_PI * 0.0125f;
	}
	else {
		static ULONGLONG timeStart = 0;
		ULONGLONG timeCurrent = GetTickCount64();
		if (timeStart == 0) {
			timeStart = timeCurrent;
		}
		t = (timeCurrent - timeStart) / 1000.0f;
	}

	g_mWorld = XMMatrixRotationY(t);

	// reset light dir
	g_vLightDirs[1] = XMFLOAT4(0.0f, 0.0f, -1.0f, 1.0f);

	// Rotate the second light around the origin
	XMMATRIX mRotate = XMMatrixRotationY(-2.0f * t);
	XMVECTOR vLightDir = XMLoadFloat4(&g_vLightDirs[1]);
	vLightDir = XMVector3Transform(vLightDir, mRotate);
	XMStoreFloat4(&g_vLightDirs[1], vLightDir);
}

/*
* Rendering the frame
*/

void render() {
	g_pdxContext->ClearRenderTargetView(g_pdxRenderTargetView, Colors::MidnightBlue);
	g_pdxContext->ClearDepthStencilView(g_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	//update constant buffer values 
	ConstantBuffer cb;
	cb.mWorld = XMMatrixTranspose(g_mWorld);
	cb.mView = XMMatrixTranspose(g_mView);
	cb.mPprojection = XMMatrixTranspose(g_mProjection);
	cb.vLightDir[0] = g_vLightDirs[0];
	cb.vLightDir[1] = g_vLightDirs[1];
	cb.vLightColor[0] = g_vLightColors[0];
	cb.vLightColor[1] = g_vLightColors[1];
	cb.vOutputColor = XMFLOAT4(0, 0, 0, 0);
	g_pdxContext->UpdateSubresource(g_pConstantBuffer, 0, nullptr, &cb, 0, 0);

	//rendering 
	g_pdxContext->VSSetShader(g_pVertexShader, nullptr, 0);
	g_pdxContext->VSSetConstantBuffers(0, 1, &g_pConstantBuffer);
	g_pdxContext->PSSetShader(g_pPixelShader, nullptr, 0);
	g_pdxContext->PSSetConstantBuffers(0, 1, &g_pConstantBuffer);
	g_pdxContext->DrawIndexed(36, 0, 0);

	//rendering lights
	for (int i = 0; i < 2; i++) {
		XMMATRIX mLight = XMMatrixTranslationFromVector(5.0f * XMLoadFloat4(&g_vLightDirs[i]));
		XMMATRIX mLightScale = XMMatrixScaling(0.2f, 0.2f, 0.2f);
		mLight = mLightScale * mLight;
		
		//updateing the world vairable to reflect the current light
		cb.mWorld = XMMatrixTranspose(mLight);
		cb.vOutputColor = g_vLightColors[i];
		g_pdxContext->UpdateSubresource(g_pConstantBuffer, 0, nullptr, &cb, 0, 0);

		g_pdxContext->PSSetShader(g_pPixelShaderSolid, nullptr, 0);
		g_pdxContext->DrawIndexed(36, 0, 0);
	}

	g_pdxSwapChain->Present(0, 0);
}

/*
* disposing
*/

void dispose() {
	if (g_pdxContext) g_pdxContext->ClearState();

	if (g_pConstantBuffer) g_pConstantBuffer->Release();
	if (g_pVertexBuffer) g_pVertexBuffer->Release();
	if (g_pIndexBuffer) g_pIndexBuffer->Release();
	if (g_pVertexLayout) g_pVertexLayout->Release();
	if (g_pVertexShader) g_pVertexShader->Release();
	if (g_pPixelShader) g_pPixelShader->Release();
	if (g_pPixelShaderSolid) g_pPixelShaderSolid->Release();

	if (g_pdxRenderTargetView) g_pdxRenderTargetView->Release();
	if (g_pDepthStencil) g_pDepthStencil->Release();
	if (g_pDepthStencilView) g_pDepthStencilView->Release();

	if (g_pdxSwapChain) g_pdxSwapChain->Release();
	if (g_pdxSwapChain1) g_pdxSwapChain1->Release();
	if (g_pdxContext) g_pdxContext->Release();
	if (g_pdxContext1) g_pdxContext1->Release();
	if (g_pdxDevice) g_pdxDevice->Release();
	if (g_pdxDevice1) g_pdxDevice1->Release();

}